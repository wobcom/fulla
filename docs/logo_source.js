// this uses p5js and p5js-svg
function setup() {
  createCanvas(100, 100, SVG);
  strokeWeight(5);
  stroke(0);
  randomSeed(12439); // comment for curation mode
}

function draw() {
  for (let rad = 0; rad < 50; rad += 15) {
    for (_ = 0; _ < rad; _++) {
      var angle = random(0, 1)*Math.PI*2;
      point(Math.cos(angle)*rad+50, Math.sin(angle)*rad+50);
    }
  }

  save("logo.svg");
  noLoop();
}
