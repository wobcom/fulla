// this uses p5js and p5js-svg
function setup() {
  createCanvas(350, 100, SVG);
  strokeWeight(5);
  stroke(0);
  fill(0);
  randomSeed(12439); // comment for curation mode
  textSize(60);
  textFont("Courier New");
}

function draw() {
  for (let rad = 0; rad < 50; rad += 15) {
    for (_ = 0; _ < rad; _++) {
      var angle = random(0, 1)*Math.PI*2;
      point(Math.cos(angle)*rad+50, Math.sin(angle)*rad+50);
    }
  }
  strokeWeight(1);
  text("fulla", 110, 65);

  save("logo_with_text.svg");
  noLoop();
}
