![logo](/docs/logo_with_text.svg)

Fulla is a lesser known Norse Goddess. Her name means “bountiful”,
and she watches over our device ecosystem.

We use Fulla to manage our CPE deployments. Her role is a double
role: a CRM for our devices, and an automated provisioning system.

It is currently in a very early stage, and you probably want to
check back in a few weeks’ time.

Fulla is licensed under GPLv3.

## Introduction

Fulla is a three part system: it is an API, a SPA frontend, and a
task runner. Those systems are relatively decoupled, and their
architecture looks roughly like this:

TODO: add diagram

You can run the API on its own to just manage data, or just run the
API and the task runner to integrate the system into your frontend.
We suggest running the whole system, though, to have the UI at least
as a fallback.

## Installation

Currently you will have to check out the repository and run the
frontend and backend manually (see the [`Development`](#development)
section to understand how to do that).

As such, this is not yet meant as a public release. We welcome any
and all contributions and feedback, though!

## Development

To get started, you will need `yarn` and `pipenv` for the frontend
and backend, respectively. Knowledge of working with both Django and
React applications will help you.

### Setup

For a full development experience with all three services running,
you will need at least four processes:

- A frontend server: to start it go to the `frontend` directory,
  call `npm install`, then `yarn start`. A frontend should run on
  port 3000.
- An API server: to start it run `pipenv shell` to get a new virtual
  environment, then go to the `backend` directory, and run `python
  manage.py migrate` (only the first time), and finally `python
  manage.py shell`. An API server should run on port 8000.
- A Redis server: you will either need a local instance or a Docker
  instance. The easiest, cross-platform solution here is Docker, you
  can start an instance using `docker run --rm -p 6379:6379 redis`.
  If you add the `-d` option it will run in the background.
- A task runner: to start it run `pipenv shell` again if you are in
  a new terminal. Go to the backend directory again and start a
  Celery task server using `celery -A backend worker -l info`.

### Architecture

If you followed the setup section, the technologies used should now
be more or less clear. Nonetheless, let’s look at it again:

- The frontend uses React. The development server runs using a
  simple Node proxy server. Ideally you’d run a real web server of
  your choice in production.
- The backend uses Django and the Django REST framework. It also
  provides a simple Django admin interface.
- The task runner uses Redis as a queue and Celery as a scheduler.
  The tasks depend on the database, so the database should be
  reachable from the test runner.

Essentially the services are decoupled, but running a frontend or
task runner without a backend does not make much sense.

You should now know about all of the fundamentals that you need to
dive into the code!

<hr/>

Have fun!
