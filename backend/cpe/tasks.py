from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from celery import shared_task

from cpe.models import CPE


@receiver(post_save, sender=CPE)
def on_cpe_save(instance=None, created=False, **kwargs):
    if created:
        provision_cpe.delay(instance.pk)
    else:
        update_cpe.delay(instance.pk)


@receiver(pre_delete, sender=CPE)
def on_cpe_delete(instance=None, **kwargs):
    decommission_cpe.delay(instance.pk, instance.serial)


@shared_task
def provision_cpe(pk):
    cpe = CPE.objects.get(pk=pk)

    return "Ran initial provisioning for CPE {}".format(pk)


@shared_task
def update_cpe(pk):
    cpe = CPE.objects.get(pk=pk)

    if not cpe.services.exists():
        return

    return cpe.services.first().generate_for(cpe)


@shared_task
def decommission_cpe(pk, serial):
    return "Ran decommission for CPE {}".format(pk)
