from django import template
from django.db import models


class Template(models.Model):
    name = models.CharField(max_length=64)
    template = models.TextField()

    def __str__(self):
        return self.name


class Variable(models.Model):
    name = models.CharField(max_length=64)
    template = models.ForeignKey(
        Template, on_delete=models.CASCADE, related_name="variables"
    )


class Service(models.Model):
    template = models.ForeignKey(
        Template, on_delete=models.CASCADE, related_name="services"
    )

    def __str__(self):
        return self.template.name

    def generate_for(self, cpe):
        t = template.Template("{% load template_extras %}" + self.template.template)

        ctx = template.Context({"cpe": cpe})
        for v in self.values.all():
            ctx[v.variable.name] = v.value

        return t.render(ctx)


class Value(models.Model):
    service = models.ForeignKey(
        Service, on_delete=models.CASCADE, related_name="values"
    )
    variable = models.ForeignKey(
        Variable, on_delete=models.CASCADE, related_name="values"
    )
    value = models.CharField(max_length=256)


class Customer(models.Model):
    identifier = models.CharField(max_length=128)

    def __str__(self):
        return self.identifier


class CPE(models.Model):
    management_ip = models.GenericIPAddressField(blank=True, null=True)
    serial = models.CharField(max_length=256)
    name = models.CharField(max_length=256)
    description = models.TextField()
    services = models.ManyToManyField(Service, related_name="cpes")
    customer = models.ForeignKey(
        Customer, blank=True, null=True, on_delete=models.SET_NULL, related_name="cpes"
    )
    status = models.CharField(
        max_length=20,
        choices=(
            ("online", "Online"),
            ("maintenance_needed", "Maintenance Needed"),
            ("offline", "Offline"),
        ),
    )

    def __str__(self):
        return "{} ({})".format(self.name, self.serial)

    class Meta:
        verbose_name = "CPE"
        verbose_name_plural = "CPEs"
