from rest_framework import routers

from cpe import views

router = routers.DefaultRouter()

router.register("templates", views.TemplateView)
router.register("variables", views.VariableView)
router.register("values", views.ValueView)
router.register("services", views.ServiceView)
router.register("customers", views.CustomerView)
router.register("cpes", views.CPEView)

app_name = "cpe"
urlpatterns = router.urls
