from django.http import HttpResponseNotAllowed
from django.shortcuts import render
from rest_framework import viewsets

from cpe import serializers, models, tasks


class TemplateView(viewsets.ModelViewSet):
    serializer_class = serializers.TemplateSerializer
    queryset = models.Template.objects.all()


class VariableView(viewsets.ModelViewSet):
    serializer_class = serializers.VariableSerializer
    queryset = models.Variable.objects.all()


class ValueView(viewsets.ModelViewSet):
    serializer_class = serializers.ValueSerializer
    queryset = models.Value.objects.all()
    http_method_names = ["get", "post", "patch", "head", "delete"]  # no put!


class ServiceView(viewsets.ModelViewSet):
    serializer_class = serializers.ServiceSerializer
    queryset = models.Service.objects.all()


class CustomerView(viewsets.ModelViewSet):
    serializer_class = serializers.CustomerSerializer
    queryset = models.Customer.objects.all()


class CPEView(viewsets.ModelViewSet):
    serializer_class = serializers.CPESerializer
    queryset = models.CPE.objects.all()
