from django.contrib import admin

from cpe import models


class CPEAdmin(admin.ModelAdmin):
    list_display = ("name", "serial", "customer", "status")


# Register your models here.
admin.site.register(models.Service)
admin.site.register(models.Customer)
admin.site.register(models.Template)
admin.site.register(models.CPE, CPEAdmin)
