# Generated by Django 3.1a1 on 2020-05-19 14:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("cpe", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="cpe",
            name="management_ip",
            field=models.GenericIPAddressField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="cpe",
            name="customer",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="cpes",
                to="cpe.customer",
            ),
        ),
        migrations.AlterField(
            model_name="cpe",
            name="services",
            field=models.ManyToManyField(related_name="cpes", to="cpe.Service"),
        ),
        migrations.AlterField(
            model_name="service",
            name="template",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="services",
                to="cpe.template",
            ),
        ),
        migrations.AlterField(
            model_name="value",
            name="service",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="values",
                to="cpe.service",
            ),
        ),
        migrations.AlterField(
            model_name="value",
            name="variable",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="values",
                to="cpe.variable",
            ),
        ),
        migrations.AlterField(
            model_name="variable",
            name="template",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="variables",
                to="cpe.template",
            ),
        ),
    ]
