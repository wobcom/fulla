from django.db.models import Count
from django.db.models.signals import post_save, pre_delete
from django.test import TestCase
from rest_framework.test import APIClient

from cpe import models, tasks


class ModelTestCase(TestCase):
    fixtures = ["cpe"]

    def test_template(self):
        template = models.Template.objects.first()
        self.assertEqual(template.name, str(template))

    def test_service(self):
        service = models.Service.objects.first()

        self.assertEqual(
            service.generate_for(service.cpes.first()), "noop 1\n  12345 01"
        )

        self.assertEqual(service.template.name, str(service))

    def test_customer(self):
        customer = models.Customer.objects.first()
        self.assertEqual(customer.identifier, str(customer))

    def test_cpe(self):
        cpe = models.CPE.objects.first()
        self.assertEqual("{} ({})".format(cpe.name, cpe.serial), str(cpe))


class CPETestCase(TestCase):
    fixtures = ["cpe"]

    def __init__(self, *args, **kwargs):
        # no redis in our tests
        post_save.disconnect(tasks.on_cpe_save, sender=models.CPE)
        pre_delete.disconnect(tasks.on_cpe_delete, sender=models.CPE)

        return super().__init__(*args, **kwargs)

    def setUp(self):
        self.client = APIClient()


class CustomerTest(CPETestCase):
    def test_list(self):
        customers = models.Customer.objects
        response = self.client.get("/api/customers/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), customers.count())

        for c1, c2 in zip(response.data, customers.all()):
            self.assertEqual(c1["id"], c2.id)
            self.assertEqual(c1["identifier"], c2.identifier)

    def test_get(self):
        customer = models.Customer.objects.first()
        response = self.client.get(f"/api/customers/{customer.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], customer.id)
        self.assertEqual(response.data["identifier"], customer.identifier)

    def test_post(self):
        customer = {"identifier": "test customer"}
        response = self.client.post(f"/api/customers/", data=customer)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["identifier"], "test customer")

    def test_put(self):
        id_ = models.Customer.objects.first().id
        customer = {"identifier": "updated customer"}
        response = self.client.put(f"/api/customers/{id_}/", data=customer)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["identifier"], "updated customer")

    def test_delete(self):
        customers = models.Customer.objects
        count = customers.count()
        id_ = customers.first().id
        response = self.client.delete(f"/api/customers/{id_}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get("/api/customers/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), count - 1)
        self.assertEqual(len(response.data), customers.count())


class TemplateTest(CPETestCase):
    def test_list(self):
        templates = models.Template.objects
        response = self.client.get("/api/templates/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), templates.count())

        for c1, c2 in zip(response.data, templates.all()):
            self.assertEqual(c1["id"], c2.id)
            self.assertEqual(c1["name"], c2.name)
            self.assertEqual(c1["template"], c2.template)

    def test_get(self):
        template = models.Template.objects.first()
        response = self.client.get(f"/api/templates/{template.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], template.id)
        self.assertEqual(response.data["name"], template.name)
        self.assertEqual(response.data["template"], template.template)

    def test_post(self):
        template = {"name": "test template", "template": "baz"}
        response = self.client.post(f"/api/templates/", data=template)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "test template")
        self.assertEqual(response.data["template"], "baz")

    def test_put(self):
        id_ = models.Template.objects.first().id
        template = {"name": "new name", "template": "new template"}
        response = self.client.put(f"/api/templates/{id_}/", data=template)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["name"], "new name")
        self.assertEqual(response.data["template"], "new template")

    def test_delete(self):
        templates = models.Template.objects
        count = templates.count()
        id_ = templates.first().id
        response = self.client.delete(f"/api/templates/{id_}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get("/api/templates/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), count - 1)
        self.assertEqual(len(response.data), templates.count())


class VariableTest(CPETestCase):
    def test_list(self):
        variables = models.Variable.objects
        response = self.client.get("/api/variables/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), variables.count())

        for c1, c2 in zip(response.data, variables.all()):
            self.assertEqual(c1["id"], c2.id)
            self.assertEqual(c1["name"], c2.name)
            self.assertEqual(c1["template"], c2.template.id)

    def test_get(self):
        variable = models.Variable.objects.first()
        response = self.client.get(f"/api/variables/{variable.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], variable.id)
        self.assertEqual(response.data["name"], variable.name)
        self.assertEqual(response.data["template"], variable.template.id)

    def test_post(self):
        template = models.Variable.objects.first()
        variable = {"name": "test variable", "template": template.id}
        response = self.client.post(f"/api/variables/", data=variable)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "test variable")
        self.assertEqual(response.data["template"], template.id)

    def test_put(self):
        id_ = models.Variable.objects.first().id
        template_id = models.Template.objects.first().id
        variable = {"name": "new name", "template": template_id}
        response = self.client.put(f"/api/variables/{id_}/", data=variable)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["name"], "new name")
        self.assertEqual(response.data["template"], template_id)

    def test_delete(self):
        variables = models.Variable.objects
        count = variables.count()
        id_ = variables.first().id
        response = self.client.delete(f"/api/variables/{id_}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get("/api/variables/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), count - 1)
        self.assertEqual(len(response.data), variables.count())


class ValueTest(CPETestCase):
    def test_list(self):
        values = models.Value.objects
        response = self.client.get("/api/values/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), values.count())

        for c1, c2 in zip(response.data, values.all()):
            self.assertEqual(c1["id"], c2.id)
            self.assertEqual(c1["value"], c2.value)

    def test_get(self):
        value = models.Value.objects.first()
        response = self.client.get(f"/api/values/{value.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], value.id)
        self.assertEqual(response.data["value"], value.value)

    def test_patch(self):
        id_ = models.Value.objects.first().id
        value = {"value": "new value"}
        response = self.client.patch(f"/api/values/{id_}/", data=value)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["value"], "new value")

    def test_put_not_allowed(self):
        id_ = models.Value.objects.first().id
        response = self.client.put(f"/api/values/{id_}/", data={})

        self.assertEqual(response.status_code, 405)

    def test_delete(self):
        values = models.Value.objects
        count = values.count()
        id_ = values.first().id
        response = self.client.delete(f"/api/values/{id_}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get("/api/values/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), count - 1)
        self.assertEqual(len(response.data), values.count())


class ServiceTest(CPETestCase):
    template_query = models.Template.objects.annotate(num_vars=Count("variables"))

    def test_list(self):
        services = models.Service.objects
        response = self.client.get("/api/services/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), services.count())

        for c1, c2 in zip(response.data, services.all()):
            self.assertEqual(c1["id"], c2.id)
            self.assertEqual(c1["template"]["id"], c2.template.id)

    def test_get(self):
        service = models.Service.objects.first()
        response = self.client.get(f"/api/services/{service.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], service.id)
        self.assertEqual(response.data["template"]["id"], service.template.id)

    def test_post(self):
        cpe = models.CPE.objects.first()
        template = self.template_query.filter(num_vars=0).first()
        service = {"template": {"id": template.id}, "cpe": cpe.id}
        response = self.client.post(f"/api/services/", data=service, format="json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["template"]["id"], template.id)
        self.assertEqual(response.data["values"], [])
        self.assertTrue(
            models.CPE.objects.first().services.filter(pk=response.data["id"]).exists()
        )

    def test_post_with_vars(self):
        with_vars = self.template_query.filter(num_vars__gt=0).first()
        service = {"template": {"id": with_vars.id}}
        response = self.client.post(f"/api/services/", data=service, format="json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["template"]["id"], with_vars.id)
        self.assertEqual(len(response.data["values"]), 1)
        for value in response.data["values"]:
            self.assertEqual(
                1, with_vars.variables.filter(name=value["variable"]["name"]).count()
            )
            self.assertEqual(value["value"], "")

    def test_post_values_fails(self):
        template = self.template_query.first()
        service = {"template": {"id": template.id}, "values": []}
        response = self.client.post(f"/api/services/", data=service, format="json")

        self.assertEqual(response.status_code, 400)

    def test_put(self):
        id_ = models.Service.objects.first().id
        template_id = self.template_query.first().id
        service = {"template": {"id": template_id}}
        response = self.client.put(f"/api/services/{id_}/", data=service, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["template"]["id"], template_id)

    def test_delete(self):
        services = models.Service.objects
        count = services.count()
        id_ = services.first().id
        response = self.client.delete(f"/api/services/{id_}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get("/api/services/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), count - 1)
        self.assertEqual(len(response.data), services.count())


class CPETest(CPETestCase):
    def test_list(self):
        cpes = models.CPE.objects
        response = self.client.get("/api/cpes/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), cpes.count())

        for c1, c2 in zip(response.data, cpes.all()):
            self.assertEqual(c1["id"], c2.id)
            self.assertEqual(c1["name"], c2.name)

    def test_get(self):
        cpe = models.CPE.objects.first()
        response = self.client.get(f"/api/cpes/{cpe.id}/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], cpe.id)
        self.assertEqual(response.data["name"], cpe.name)

    def test_post(self):
        cpe = {
            "name": "my name",
            "serial": "54321",
            "description": "my thing",
            "status": "online",
            "customer": {
                "id": models.Customer.objects.first().id,
                "identifier": models.Customer.objects.first().identifier,
            },
            "services": [],
        }
        response = self.client.post(f"/api/cpes/", data=cpe, format="json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "my name")
        self.assertEqual(1, models.CPE.objects.filter(name="my name").count())

    def test_put(self):
        cpe = models.CPE.objects.first()
        id_ = cpe.id
        cpe = {
            "name": "updated name",
            "serial": cpe.serial,
            "description": cpe.description,
            "status": cpe.status,
            "customer": {"id": cpe.customer.id, "identifier": cpe.customer.identifier},
            "services": [],
        }
        response = self.client.put(f"/api/cpes/{id_}/", data=cpe, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["name"], "updated name")
        self.assertEqual(id_, models.CPE.objects.get(name="updated name").id)

    def test_patch(self):
        id_ = models.CPE.objects.first().id
        cpe = {"name": "updated name"}
        response = self.client.patch(f"/api/cpes/{id_}/", data=cpe, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["id"], id_)
        self.assertEqual(response.data["name"], "updated name")
        self.assertEqual(id_, models.CPE.objects.get(name="updated name").id)

    def test_delete(self):
        cpes = models.CPE.objects
        count = cpes.count()
        id_ = cpes.first().id
        response = self.client.delete(f"/api/cpes/{id_}/")

        self.assertEqual(response.status_code, 204)

        response = self.client.get("/api/cpes/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), count - 1)
        self.assertEqual(len(response.data), cpes.count())
