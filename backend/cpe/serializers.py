from rest_framework import serializers

from cpe.models import Service, Template, Variable, Value, Customer, CPE


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ("id", "identifier")


class NestedCustomerSerializer(CustomerSerializer):
    id = serializers.IntegerField()


class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = ("id", "name", "template")


class NestedVariableSerializer(VariableSerializer):
    id = serializers.IntegerField()


class ValueSerializer(serializers.ModelSerializer):
    variable = NestedVariableSerializer(read_only=True)
    service_id = serializers.IntegerField()

    class Meta:
        model = Value
        fields = ("id", "variable", "value", "service_id")


class NestedValueSerializer(ValueSerializer):
    id = serializers.IntegerField(required=False)


class TemplateSerializer(serializers.ModelSerializer):
    variables = VariableSerializer(many=True, required=False)

    class Meta:
        model = Template
        fields = ("id", "name", "template", "variables")


class NestedTemplateSerializer(TemplateSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Template
        fields = ("id", "name", "template", "variables")
        read_only_fields = ["name", "template", "variabes"]


class ServiceSerializer(serializers.ModelSerializer):
    template = NestedTemplateSerializer()
    values = NestedValueSerializer(many=True, required=False)
    cpe = serializers.IntegerField(required=False)

    def create(self, validated_data):
        if "values" in validated_data:
            raise serializers.ValidationError("Cannot set values on service creation!")
        template = validated_data.pop("template")
        cpe = validated_data.pop("cpe", None)
        validated_data["template_id"] = template["id"]
        instance = Service.objects.create(**validated_data)

        for variable in instance.template.variables.all():
            Value.objects.create(variable=variable, value="", service=instance)

        if cpe:
            instance.cpes.add(cpe)

        return instance

    def update(self, instance, validated_data):
        values_data = validated_data.pop("values", [])
        template = validated_data.pop("template")

        values_serializer = NestedValueSerializer()
        for value in values_data:
            value_instance = Value.objects.get(pk=value["id"])
            if value_instance.service.id != instance.id:
                raise serializers.ValidationError(
                    f"Value {value['id']} does not belong to service"
                )
            values_serializer.update(value_instance, {"value": value["value"]})

        instance.template = Template.objects.get(pk=template["id"])
        instance.save()

        super().update(instance, validated_data)

        return instance

    class Meta:
        model = Service
        fields = ("id", "template", "values", "cpe")


class CPESerializer(serializers.ModelSerializer):
    services = ServiceSerializer(many=True)
    customer = NestedCustomerSerializer()

    def create(self, validated_data):
        services_data = validated_data.pop("services")
        customer = validated_data.pop("customer")
        instance = CPE.objects.create(**validated_data)

        service_serializer = self.fields["services"]
        for service in services_data:
            service["cpe"] = instance
        service_serializer.create(services_data)

        Customer.objects.get(pk=customer["id"]).cpes.add(instance)

        return instance

    def update(self, instance, validated_data):
        if "services" in validated_data:
            _ = validated_data.pop("services")

        if "customer" in validated_data:
            customer = validated_data.pop("customer")

            instance.customer = Customer.objects.get(pk=customer["id"])
            instance.save()

        super().update(instance, validated_data)

        return instance

    class Meta:
        model = CPE
        fields = (
            "id",
            "serial",
            "name",
            "description",
            "services",
            "customer",
            "status",
        )
