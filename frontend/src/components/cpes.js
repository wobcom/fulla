import React from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { Button, ButtonGroup, Spinner } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

import SearchBar from './searchbar';
import { removeCPE, fetchCPEs } from '../redux/actions';

class CPEs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBar: '',
    };
  }

  componentDidMount() {
    this.props.fetchCPEs();
  }

  renderSearch = () => {
    return (
      <SearchBar onChange={(e) => this.setState({searchBar: e.target.value})}/>
    );
  }

  renderStatus = (status) => {
    const statusDefs = {
      'online': {
        'color': 'green',
        'value': 'Online',
      },
      'offline': {
        'color': 'red',
        'value': 'Offline',
      },
      'maintenance_needed': {
        'color': 'yellow',
        'value': 'Warning',
      },
    };

    return (
      <div>
        <div style={{display: 'inline-block', backgroundColor: statusDefs[status].color, width: '1em', height: '1em', marginRight: '10px', borderRadius: '50%'}}/>
        <span>{statusDefs[status].value}</span>
      </div>
    );
  }

  renderItem = (item) => {
    return (
      <tr key={item.id}>
        <td>{this.renderStatus(item.status)}</td>
        <td>{item.customer ? item.customer.identifier : "None"}</td>
        <td>{item.name}</td>
        <td>{item.services.length}</td>
        <td className="text-right">
          <ButtonGroup>
            <Link className="btn btn-warning" to={{
              pathname: `/cpes/${item.id}`,
              state: {
                cpes: this.props.cpes,
              },
            }}>
              <FontAwesomeIcon icon={faPen} />
            </Link>
            <Button color="danger" onClick={() => this.props.removeCPE(item.id)}>
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </ButtonGroup>
        </td>
      </tr>
    );
  };

  renderItems = () => {
    const { searchBar } = this.state;
    const items = this.props.cpes.filter(cpe => SearchBar.fuzzy(cpe.name, searchBar));
    if (this.props.loading) return <Spinner color="secondary"/>
    return (
      <table className="table table-hover table-headings">
        <thead>
          <tr>
            <th>Status</th>
            <th>Customer</th>
            <th>Name</th>
            <th>Service Count</th>
            <th>{this.renderSearch()}</th>
          </tr>
        </thead>
        <tbody>
          {items.map(this.renderItem)}
        </tbody>
      </table>
    )
  }

  render() {
    return (
      <div className="row">
        {this.renderItems()}
        <Link className="btn btn-success" to={{
          pathname: `/cpes/new`,
          state: {
            cpes: this.props.cpes,
          },
        }}>
          <FontAwesomeIcon icon={faPlus} /> Add CPE
        </Link>
      </div>
    );
  }
}

export default connect(
  state => ({
    cpes: state.cpes.cpes,
    loading: state.cpes.loading,
  }),
  {removeCPE, fetchCPEs}
)(CPEs);
