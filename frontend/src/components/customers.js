import React from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { Button, ButtonGroup, Spinner } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

import SearchBar from './searchbar';
import { removeCustomer, fetchCustomers } from '../redux/actions';

class Customers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBar: '',
    };
  }

  componentDidMount() {
    this.props.fetchCustomers();
  }

  renderSearch = () => {
    return (
      <SearchBar onChange={(e) => this.setState({searchBar: e.target.value})}/>
    );
  }

  renderItem = (item) => {
    return (
      <tr key={item.id}>
        <td>{item.identifier}</td>
        <td className="text-right">
          <ButtonGroup>
            <Link className="btn btn-warning" to={{
              pathname: `/customers/${item.id}`,
            }}>
              <FontAwesomeIcon icon={faPen} />
            </Link>
            <Button color="danger" onClick={() => this.props.removeCustomer(item.id)}>
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </ButtonGroup>
        </td>
      </tr>
    );
  };

  renderItems = () => {
    const { searchBar } = this.state;
    const items = this.props.customers.filter(customer => SearchBar.fuzzy(customer.identifier, searchBar));
    if (this.props.loading) return <Spinner color="secondary"/>
    return (
      <table className="table table-hover table-headings">
        <thead>
          <tr>
            <th>Name</th>
            <th>{this.renderSearch()}</th>
          </tr>
        </thead>
        <tbody>
          {items.map(this.renderItem)}
        </tbody>
      </table>
    )
  }

  render() {
    return (
      <div className="row">
        {this.renderItems()}
        <Link className="btn btn-success" to={{
          pathname: `/customers/new`,
          state: {
            customers: this.props.customers,
          },
        }}>
          <FontAwesomeIcon icon={faPlus} /> Add Customer
        </Link>
      </div>
    );
  }
}

export default connect(
  state => ({
    customers: state.customers.customers,
    loading: state.customers.loading,
  }),
  {removeCustomer, fetchCustomers}
)(Customers);
