import React from 'react';
import { Link } from "react-router-dom";
import { Table, Button, ButtonGroup, Form, FormGroup, Input, Label, Spinner } from "reactstrap";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

import { addCPE, removeService, fetchCPEs, fetchCustomers, updateCPE, apiError } from '../redux/actions';

class CPE extends React.Component {
  constructor(props) {
    super(props);
    const { cpeId } = props.match.params;

    this.state = {
      cpe: null,
      cpeId: cpeId,
    };
  }

  componentDidMount() {
    this.props.fetchCPEs();
    this.props.fetchCustomers();
  }

  static getDerivedStateFromProps(props, state) {
    if (state.loading) return {...state, cpe: null};
    if (!state.cpeId && !state.cpe) return {cpe: { status: 'online', customer: props.customers[0] || {'id': 0}, name: '', serial: '', description: '', services: []}};
    if (state.cpe && !state.cpe.customer.id && props.customers.length) state.cpe.customer = props.customers[0];
    const cpeList = props.cpes.filter(cpe => cpe.id === Number(state.cpeId));
    const cpe = cpeList[0];
    if (!cpe) return state;
    if (state.cpe) {
      state.cpe.services = cpe.services;
      return state;
    }
    return {
      cpe: cpe,
    };
  }

  handleChange = e => {
    let { name, value } = e.target;
    const cpe = { ...this.state.cpe, [name]: value };
    this.setState({ cpe });
  };

  handleCustomerChange = e => {
    let { value } = e.target;
    const customer = this.props.customers.filter(c => c.id === Number(value))[0];
    const cpe = { ...this.state.cpe, customer };
    this.setState({ cpe });
  };

  submit = () => {
    if (this.state.cpeId) this.props.updateCPE(this.state.cpe);
    else this.props.addCPE(this.state.cpe);
    this.props.history.push('/');
  }

  error = (message) => {
    this.props.apiError(message);
    this.props.history.push('/');
    return null;
  }

  addService = (id) => {
    this.props.history.push(`/services/${this.state.cpe.id}`);
  }

  editService = (id) => {
    this.props.history.push(`/services/${this.state.cpe.id}/${id}`);
  }

  removeService(id) {
    this.props.removeService(id);
  }


  renderService(service) {
    const getVar = varid => service.values.filter(v => v.variable.id === varid)[0];
    return (
      <tr key={service.id}>
        <td>{service.template.name}</td>
        <td>{service.template.variables.map(v => `${v.name}=${getVar(v.id).value}`).join(", ")}</td>
        <td className="text-right">
          <ButtonGroup>
            <Button color="warning" onClick={() => this.editService(service.id)}>
              <FontAwesomeIcon icon={faPen} />
            </Button>
            <Button color="danger" onClick={() => this.removeService(service.id)}>
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </ButtonGroup>
        </td>
      </tr>
    );
  }

  ready() {
    const { cpe } = this.state;
    return cpe.status && cpe.customer.id && cpe.name && cpe.serial && cpe.description;
  }

  renderServices() {
    if (!this.state.cpeId) return <p>You will need to create the CPE before you can add services.</p>;
    if (!this.state.cpe.services.length) return <p>There are no services configured on this CPE.</p>;
    return(
      <Table hover responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th>Variables</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.state.cpe.services.map(s => this.renderService(s))}
        </tbody>
      </Table>
    );
  }

  render() {
    if (this.props.loading) return <Spinner color="secondary"/>;
    if (this.state.cpeId && !this.state.cpe) return this.error(`CPE ${this.state.cpeId} not found.`);

    return (
      <div className="model-form">
        <h1 className="form-heading">{this.state.cpeId ? <span>CPE {this.state.cpe.name} ({this.state.cpe.id})</span> : "Creating a CPE"}</h1>
        <Form>
          <FormGroup>
            <Label for="name">Name</Label>
            <Input
              type="text"
              name="name"
              value={this.state.cpe.name}
              onChange={this.handleChange}
              placeholder="Name"
            />
          </FormGroup>
          <FormGroup>
            <Label for="serial">Serial Number</Label>
            <Input
              type="text"
              name="serial"
              value={this.state.cpe.serial}
              onChange={this.handleChange}
              placeholder="Serial Number"
            />
          </FormGroup>
          <FormGroup>
            <Label for="management_ip">Management IP</Label>
            <Input
              type="text"
              name="management_ip"
              value={this.state.cpe.management_ip}
              onChange={this.handleChange}
              placeholder="IPv4/IPv6"
            />
          </FormGroup>
          <FormGroup>
            <Label for="description">Description</Label>
            <Input
              type="textarea"
              name="description"
              value={this.state.cpe.description}
              onChange={this.handleChange}
              placeholder="CPE description"
            />
          </FormGroup>
          <FormGroup>
            <Label for="customer">Customer</Label>
            <Input
              type="select"
              name="customer"
              value={this.state.cpe.customer.id}
              onChange={this.handleCustomerChange}
              placeholder="Customer">
              {this.props.customers.map(c => <option key={c.id} value={c.id}>{c.identifier}</option>)}
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="status">Status</Label>
              <Input
                type="select"
                name="status"
                value={this.state.cpe.status}
                onChange={this.handleChange}>
                <option value="online">Online</option>
                <option value="offline">Offline</option>
                <option value="maintenance_needed">Maintenance Needed</option>
              </Input>
          </FormGroup>
        </Form>
        <h3>Services</h3>
        {this.renderServices()}
        {this.state.cpeId &&
          <div>
            <Button color="success" onClick={this.addService}>
              <FontAwesomeIcon icon={faPlus} /> Add Service
            </Button>
          </div>}
        <div className="form-buttons">
          { !this.ready() && <p>Please provide all fields.</p> }
          <Button color="primary" disabled={!this.ready()} onClick={this.submit}>Submit</Button>
          <Link className="btn btn-secondary" to={`/cpes/`}>Cancel</Link>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    cpes: state.cpes.cpes,
    customers: state.customers.customers,
    loading: state.cpes.loading || state.customers.loading,
  }),
  {addCPE, removeService, fetchCPEs, fetchCustomers, updateCPE, apiError}
)(CPE);
