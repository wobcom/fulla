import React from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { Button, ButtonGroup, Spinner } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

import SearchBar from './searchbar';
import { addTemplate, removeTemplate, fetchTemplates } from '../redux/actions';

class Templates extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBar: '',
    };
  }

  componentDidMount() {
    this.props.fetchTemplates();
  }

  renderSearch = () => {
    return (
      <SearchBar onChange={(e) => this.setState({searchBar: e.target.value})}/>
    );
  }

  renderItem = (item) => {
    return (
      <tr key={item.id}>
        <td>{item.name}</td>
        <td className="text-right">
          <ButtonGroup>
            <Link className="btn btn-warning" to={{
              pathname: `/templates/${item.id}`,
              state: {
                templates: this.props.templates,
              },
            }}>
              <FontAwesomeIcon icon={faPen} />
            </Link>
            <Button color="danger" onClick={() => this.props.removeTemplate(item.id)}>
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </ButtonGroup>
        </td>
      </tr>
    );
  };

  renderItems = () => {
    const { searchBar } = this.state;
    const items = this.props.templates.filter(template => SearchBar.fuzzy(template.name, searchBar));
    if (this.props.loading) return <Spinner color="secondary"/>
    return (
      <table className="table table-hover table-headings">
        <thead>
          <tr>
            <th>Name</th>
            <th>{this.renderSearch()}</th>
          </tr>
        </thead>
        <tbody>
          {items.map(this.renderItem)}
        </tbody>
      </table>
    )
  }

  render() {
    return (
      <div className="row">
        {this.renderItems()}
        <Link className="btn btn-success" to={{
          pathname: `/templates/new`,
          state: {
            templates: this.props.templates,
          },
        }}>
          <FontAwesomeIcon icon={faPlus} /> Add Template
        </Link>
      </div>
    );
  }
}

export default connect(
  state => ({
    templates: state.templates.templates,
    loading: state.templates.loading,
  }),
  {fetchTemplates, addTemplate, removeTemplate}
)(Templates);
