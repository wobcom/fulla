import React from 'react';
import { connect } from 'react-redux';
import { Button, Spinner } from 'reactstrap';

import SearchBar from './searchbar';
import { fetchCPEs, fetchCustomers, fetchTemplates } from '../redux/actions';

class SearchResults extends React.Component {
  componentDidMount() {
    this.props.fetchCPEs();
    this.props.fetchCustomers();
    this.props.fetchTemplates();
  }

  /* the render method are pretty repetitive, but maybe not enough to make a HOF */

  renderCPEs() {
    const items = this.props.cpes.filter(c => SearchBar.fuzzy(c.name, this.props.forSearch));
    if (!items.length) return;
    const button = id => {
      return (
        <Button onClick={()=>{ this.props.history.push(`/cpes/${id}`); this.props.resetSearch(); }}>More</Button>
      );
    }
    return (
      <table className="table table-hover table-headings">
        <thead>
          <tr>
            <th>CPEs</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {items.map(c=><tr><td key={c.id}>{c.name}</td><td>{button(c.id)}</td></tr>)}
        </tbody>
      </table>
    );
  }

  renderCustomers() {
    const items = this.props.customers.filter(c => SearchBar.fuzzy(c.identifier, this.props.forSearch));
    if (!items.length) return;
    const button = id => {
      return (
        <Button onClick={()=>{ this.props.history.push(`/customers/${id}`); this.props.resetSearch(); }}>More</Button>
      );
    }
    return (
      <table className="table table-hover table-headings">
        <thead>
          <tr>
            <th>Customers</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {items.map(c=><tr><td key={c.id}>{c.identifier}</td><td>{button(c.id)}</td></tr>)}
        </tbody>
      </table>
    );
  }

  renderTemplates() {
    const items = this.props.templates.filter(t => SearchBar.fuzzy(t.name, this.props.forSearch));
    if (!items.length) return;
    const button = id => {
      return (
        <Button onClick={()=>{ this.props.history.push(`/templates/${id}`); this.props.resetSearch(); }}>More</Button>
      );
    }
    return (
      <table className="table table-hover table-headings">
        <thead>
          <tr>
            <th>Templates</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {items.map(c=><tr><td key={c.id}>{c.name}</td><td>{button(c.id)}</td></tr>)}
        </tbody>
      </table>
    );
  }

  render() {
    if (this.props.loading) return <Spinner color="secondary"/>

    const cpes = this.renderCPEs();
    const customers = this.renderCustomers();
    const templates = this.renderTemplates();

    return (
      <div>
        { cpes || customers || templates ?
          <div>{cpes}{customers}{templates}</div> :
          <p className="search-msg">Search returned no results.</p>
        }
      </div>
    );
  }
}


export default connect(
  state => ({
    cpes: state.cpes.cpes,
    customers: state.customers.customers,
    templates: state.templates.templates,
    loading: state.cpes.loading || state.customers.loading || state.templates.loading,
  }),
  {fetchCPEs, fetchCustomers, fetchTemplates}
)(SearchResults);
