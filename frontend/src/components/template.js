import React from 'react';
import { Link } from "react-router-dom";
import { Table, Button, InputGroup, Form, FormGroup, Input, Label, Spinner } from "reactstrap";
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';

import { addVariable, removeVariable, fetchTemplates, updateTemplate, apiError, addTemplate } from '../redux/actions';

class Template extends React.Component {
  constructor(props) {
    super(props);
    const { templateId } = props.match.params;

    this.state = {
      template: null,
      newVar: '',
      templateId: templateId,
    };
  }

  componentDidMount() {
    this.props.fetchTemplates();
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.template && !state.templateId) return {template: {name: '', variables: []}};
    if (!state.templateId) return null;
    const templateList = props.templates.filter(template => template.id === Number(state.templateId));
    const template = templateList[0];
    if (state.template && state.template.variables.length === template.variables.length) return null;
    return {
      template: template,
    };
  }

  handleChange = e => {
    let { name, value } = e.target;
    const template = { ...this.state.template, [name]: value };
    this.setState({ template });
  };

  handleVariableChange = e => {
    let { name, value } = e.target;
    const { template } = this.state;
    template.variables = template.variables.map(v => v.name === name ? {id: v.id, name, value } : v);
    this.setState({...this.state, template});
  };

  submit = () => {
    if (this.state.templateId) this.props.updateTemplate(this.state.template);
    else this.props.addTemplate(this.state.template);
    this.props.history.push('/templates');
  }

  error = (message) => {
    this.props.apiError(message);
    this.props.history.push('/templates');
    return null;
  }

  addVariable = () => {
    if (this.state.templateId) {
      this.props.addVariable(this.state.template, this.state.newVar);
    } else {
      let { template } = this.state;
      if (!template.variables) template.variables = [];
      template.variables.push({'name': this.state.newVar});
      this.setState({template});
    }
  }

  removeVariable(id) {
    return () => {
      this.props.removeVariable(id);
    };
  }

  renderVariable(variable) {
    return (
      <tr>
        <td>{variable.name}</td>
        <td className="text-right">
          <Button color="danger" onClick={this.removeVariable(variable.id)}>
            <FontAwesomeIcon icon={faTrash} />
          </Button>
        </td>
      </tr>
    );
  }

  renderVariables() {
    if (!this.state.template.variables.length) return <p>There are no variables in this template.</p>;
    return(
      <Table hover responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.state.template.variables.map(v => this.renderVariable(v))}
        </tbody>
      </Table>
    );
  }

  render() {
    if (this.props.loading) return <Spinner color="secondary"/>;
    if (!this.state.template && this.state.templateId) return this.error(`Template ${this.state.templateId} not found.`);

    return (
      <div className="model-form">
        <h1 className="form-heading">{this.state.templateId ? <span>Template {this.state.template.name} ({this.state.template.id})</span> : "Creating a Template"}</h1>
        <Form>
          <FormGroup>
            <Label for="name">Name</Label>
            <Input
              type="text"
              name="name"
              value={this.state.template.name}
              onChange={this.handleChange}
              placeholder="Name"
            />
          </FormGroup>
          <FormGroup>
            <Label for="template">Template</Label>
            <Input
              type="textarea"
              name="template"
              value={this.state.template.template}
              onChange={this.handleChange}
              placeholder="Template"
            />
          </FormGroup>
          <h3>Variables</h3>
          {this.renderVariables()}
          <InputGroup style={{maxWidth: "300px"}}>
            <Input
              type="text"
              name="newVar"
              value={this.state.newVar}
              onChange={(e) => this.setState({newVar: e.target.value})}
              placeholder="Variable Name"
            />
            <Button color="success" onClick={this.addVariable}>
              <FontAwesomeIcon icon={faPlus} /> Add
            </Button>
          </InputGroup>
        </Form>
        <div className="form-buttons">
          <Button color="primary" onClick={this.submit}>Submit</Button>
          <Link className="btn btn-secondary" to={`/templates/`}>Cancel</Link>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    templates: state.templates.templates,
    loading: state.templates.loading,
  }),
  {addTemplate, fetchTemplates, updateTemplate, addVariable, removeVariable, apiError}
)(Template);
