import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

class SearchBar extends React.Component {
  static fuzzy(text, term, ratio=0.75) {
    const string = text.toLowerCase();
    const compare = term.toLowerCase();
    let matches = 0;
    if (string.includes(compare)) return true;
    for (let i = 0; i < compare.length; i++) {
        string.includes(compare[i]) ? matches += 1 : matches -=1;
    }
    return (matches/text.length >= ratio || term === "")
  }

  render() {
    return (
      <div className="input-group list-search">
        <input type="text" className="form-control list-search" placeholder={this.props.placeholder||"Search"} onChange={this.props.onChange} value={this.props.value||''}/>
        <span className="input-group-btn">
          <button type="submit" className="btn" disabled>
            <FontAwesomeIcon icon={faSearch} />
          </button>
        </span>
      </div>
    );
  }
}

export default SearchBar;
