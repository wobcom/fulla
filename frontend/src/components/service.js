import React from 'react';
import { Link } from "react-router-dom";
import {
  Table,
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  Spinner
} from "reactstrap";
import { connect } from 'react-redux';

import {
  fetchServices,
  fetchTemplates,
  addService,
  updateService,
  apiError
} from '../redux/actions';

class Service extends React.Component {
  constructor(props) {
    super(props);
    const { cpeId, serviceId } = props.match.params;
    this.state = {
      cpeId,
      serviceId,
      service: {
        template: {
          name: "",
        },
        values: {},
      },
    };
  }

  componentDidMount() {
    this.props.fetchTemplates();
    this.props.fetchServices();
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.service.template.name && props.templates.length) state = Service.setTemplate(state, props.templates[0]);
    if (!state.serviceId || !props.services.length || state.service.id === Number(state.serviceId)) return state;
    const serviceList = props.services.filter(s => s.id === Number(state.serviceId));
    state.service = serviceList[0];
    state = Service.prepareValues(state);
    return state;
  }

  static prepareValues(state) {
    state.service.values = state.service.values.reduce((acc, v) => {acc[v.variable.name] = v; return acc;}, {});
    return state;
  }

  static setTemplate(state, template) {
    state.service.template = template;
    state.service.values = template.variables.reduce((acc, v) => {acc[v.name] = {variable: v, value: ""}; return acc;}, {});
    return state;
  }

  handleTemplateChange = e => {
    let { value } = e.target;
    const template = this.props.templates.filter(t => t.id === Number(value))[0];
    const state = Service.setTemplate(this.state, template);
    this.setState(state);
  };

  submit = () => {
    if (this.state.serviceId) this.props.updateService(this.state.serviceId, this.state.service);
    else this.props.addService(this.state.cpeId, this.state.service);
    this.props.history.push(`/cpes/${this.state.cpeId}`);
  }

  error = (message) => {
    this.props.apiError(message);
    this.props.history.push(`/cpes/${this.state.cpeId}`);
    return null;
  }

  handleValueChange = e => {
    let { name, value } = e.target;
    let state = this.state;
    state.service.values[name].value = value;
    this.setState(state);
  }

  renderValue = (value) => {
    return (
      <tr key={value}>
        <td>{value}</td>
        <td>
          <Input
            type="text"
            name={value}
            value={this.state.service.values[value].value}
            onChange={this.handleValueChange}
            placeholder="Value"/>
        </td>
      </tr>
    );
  }

  renderValues() {
    if (this.state.service.values === {}) return;

    return (
      <Table hover responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(this.state.service.values).map(this.renderValue)}
        </tbody>
      </Table>
    );
  }

  render() {
    if (this.props.loading) return <Spinner color="secondary"/>;

    return (
      <div className="model-form">
        <h1 className="form-heading">{this.state.serviceId ? "Updating" : "Creating"} Service {this.state.service.template.name}</h1>
        <Form>
          <FormGroup>
            <Label for="template">Template</Label>
            <Input
              type="select"
              name="template"
              value={this.state.service.template.id}
              onChange={this.handleTemplateChange}
              placeholder="Template">
              {this.props.templates.map(t => <option key={t.id} value={t.id}>{t.name}</option>)}
            </Input>
          </FormGroup>
        </Form>
        {this.renderValues()}
        <div className="form-buttons">
          <Button color="primary" onClick={this.submit}>Submit</Button>
          <Link className="btn btn-secondary" to={`/cpes/${this.state.cpeId}`}>Cancel</Link>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    services: state.services.services,
    templates: state.templates.templates,
    loading: state.services.loading || state.templates.loading,
  }),
  {fetchServices, fetchTemplates, addService, updateService, apiError}
)(Service);
