import React from 'react';
import { Link } from "react-router-dom";
import { Button, Form, FormGroup, Input, Label, Spinner } from "reactstrap";
import { connect } from 'react-redux';

import { addCustomer, fetchCustomers, updateCustomer, apiError } from '../redux/actions';

class Customer extends React.Component {
  constructor(props) {
    super(props);
    const { customerId } = props.match.params;

    this.state = {
      customer: null,
      customerId: customerId,
    };
  }

  componentDidMount() {
    this.props.fetchCustomers();
  }

  static getDerivedStateFromProps(props, state) {
    if (state.customer) return null;
    if (!state.customerId) return { customer: { identifier: '' } };
    const customerList = props.customers.filter(c => c.id === Number(state.customerId));
    const customer = customerList[0];
    return {
      customer: customer,
    };
  }

  handleChange = e => {
    let { name, value } = e.target;
    const customer = { ...this.state.customer, [name]: value };
    this.setState({ customer });
  };

  submit = () => {
    if (this.state.customerId) this.props.updateCustomer(this.state.customer);
    else this.props.addCustomer(this.state.customer);
    this.props.history.push('/customers');
  }

  error = (message) => {
    this.props.apiError(message);
    this.props.history.push('/customers');
    return null;
  }

  render() {
    if (this.props.loading) return <Spinner color="secondary"/>;
    if (this.state.customerId && !this.state.customer) return this.error(`Customer ${this.state.customerId} not found.`);

    return (
      <div className="model-form">
        <h1 className="form-heading">{this.state.customerId ? <span>Customer {this.state.customer.identifier} ({this.state.customer.id})</span> : "Creating a Customer"}</h1>
        <Form>
          <FormGroup>
            <Label for="identifier">Name</Label>
            <Input
              type="text"
              name="identifier"
              value={this.state.customer.identifier}
              onChange={this.handleChange}
              placeholder="Customer Name"
            />
          </FormGroup>
        </Form>
        <div className="form-buttons">
          <Button color="primary" onClick={this.submit}>Submit</Button>
          <Link className="btn btn-secondary" to={`/customers/`}>Cancel</Link>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    customers: state.customers.customers,
    loading: state.customers.loading,
  }),
  {addCustomer, fetchCustomers, updateCustomer, apiError}
)(Customer);
