import { SET_CPES, CPES_LOADING } from '../actionTypes';

const initialState = {
  cpes: [],
  loading: true,
};

export default function(state=initialState, action) {
  switch (action.type) {
    case SET_CPES: {
      const cpes = action.payload;
      return {...state, cpes};
    }
    case CPES_LOADING: {
      const loading = action.payload;
      return {...state, loading};
    }
    default:
      return state;
  }
}
