import { SET_SERVICES, SERVICES_LOADING } from '../actionTypes';

const initialState = {
  services: [],
  loading: true,
};

export default function(state=initialState, action) {
  switch (action.type) {
    case SET_SERVICES: {
      const services = action.payload;
      return {...state, services};
    }
    case SERVICES_LOADING: {
      const loading = action.payload;
      return {...state, loading};
    }
    default:
      return state;
  }
}
