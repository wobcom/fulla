import { SET_TEMPLATES, TEMPLATES_LOADING } from '../actionTypes';

const initialState = {
  templates: [],
  loading: true,
};

export default function(state=initialState, action) {
  switch (action.type) {
    case SET_TEMPLATES: {
      const templates = action.payload;
      return {...state, templates};
    }
    case TEMPLATES_LOADING: {
      const loading = action.payload;
      return {...state, loading};
    }
    default:
      return state;
  }
}
