import { SET_CUSTOMERS, CUSTOMERS_LOADING } from '../actionTypes';

const initialState = {
  customers: [],
  loading: true,
};

export default function(state=initialState, action) {
  switch (action.type) {
    case SET_CUSTOMERS: {
      const customers = action.payload;
      return {...state, customers};
    }
    case CUSTOMERS_LOADING: {
      const loading = action.payload;
      return {...state, loading};
    }
    default:
      return state;
  }
}
