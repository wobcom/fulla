import { combineReducers } from 'redux';
import api from './api';
import cpes from './cpes';
import customers from './customers';
import templates from './templates';
import services from './services';

export default combineReducers({ api, cpes, customers, templates, services });
