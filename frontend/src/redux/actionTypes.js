export const API_ERROR = "API_ERROR";

export const SET_CPES = "SET_CPES";
export const CPES_LOADING = "CPES_LOADING";
export const GET_CPES = "GET_CPES";

export const SET_CUSTOMERS = "SET_CUSTOMERS";
export const CUSTOMERS_LOADING = "CUSTOMERS_LOADING";
export const GET_CUSTOMERS = "GET_CUSTOMERS";

export const SET_TEMPLATES = "SET_TEMPLATES";
export const TEMPLATES_LOADING = "TEMPLATES_LOADING";
export const GET_TEMPLATES = "GET_TEMPLATES";

export const SET_SERVICES = "SET_SERVICES";
export const SERVICES_LOADING = "SERVICES_LOADING";
export const GET_SERVICES = "GET_SERVICES";
