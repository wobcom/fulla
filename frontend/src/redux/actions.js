import axios from "axios";

import {
  API_ERROR,
  SET_CPES,
  CPES_LOADING,
  SET_CUSTOMERS,
  CUSTOMERS_LOADING,
  SET_TEMPLATES,
  TEMPLATES_LOADING,
  SET_SERVICES,
  SERVICES_LOADING,
} from "./actionTypes";

// argh.
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN"

export const setCPEs = (cpes) => ({
  type: SET_CPES,
  payload: cpes
});

export const cpesLoading = (flag) => ({
  type: CPES_LOADING,
  payload: flag,
});

export const setCustomers = (customers) => ({
  type: SET_CUSTOMERS,
  payload: customers
});

export const customersLoading = (flag) => ({
  type: CUSTOMERS_LOADING,
  payload: flag,
});

export const setTemplates = (templates) => ({
  type: SET_TEMPLATES,
  payload: templates
});

export const templatesLoading = (flag) => ({
  type: TEMPLATES_LOADING,
  payload: flag,
});

export const setServices = (services) => ({
  type: SET_SERVICES,
  payload: services
});

export const servicesLoading = (flag) => ({
  type: SERVICES_LOADING,
  payload: flag,
});

export const apiError = (message) => ({
  type: API_ERROR,
  payload: message,
});

export const clearAPIError = () => ({
  type: API_ERROR,
  payload: null,
});

function errorMessage(err) {
  debugger;
  const response = err.response;
  if (typeof(response.data) == "string") return `HTTP ${err.response.status} ${err.response.statusText}`;
  if (typeof(response.data) == "string") return `HTTP ${err.response.status} ${err.response.statusText}`;
  if (response.data.detail) return `HTTP ${err.response.status} ${err.response.statusText}: ${err.response.data.detail}`;
  const treat = (value) => typeof(value) === "string" ? value : typeof(value.map) === "function" ? value.join(', ') : value.non_field_errors;
  const messages = Object.keys(response.data).map(k => `${k}: ${treat(response.data[k])}`).join('; ');
  return `HTTP ${err.response.status} ${err.response.statusText}: ${messages}`;
}

export function addCPE(data) {
  return function(dispatch) {
    axios.post('/api/cpes/', data)
      .then(res => dispatch(fetchCPEs()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function updateCPE(cpe) {
  return function(dispatch) {
    axios.put(`/api/cpes/${cpe.id}/`, cpe)
      .then(res => dispatch(fetchCPEs()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function fetchCPEs() {
  return function(dispatch) {
    dispatch(cpesLoading(true));
    axios.get('/api/cpes')
      .then(res => dispatch(setCPEs(res.data)))
      .catch(err => dispatch(apiError(errorMessage(err))))
      .finally(() => dispatch(cpesLoading(false)));
  }
}

export function removeCPE(id) {
  return function(dispatch) {
    axios.delete(`/api/cpes/${id}/`)
      .then(res => dispatch(fetchCPEs()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function addCustomer(customer) {
  return function(dispatch) {
    axios.post(`/api/customers/`, customer)
      .then(res => dispatch(fetchCustomers()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function updateCustomer(customer) {
  return function(dispatch) {
    axios.put(`/api/customers/${customer.id}/`, customer)
      .then(res => dispatch(fetchCustomers()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function fetchCustomers() {
  return function(dispatch) {
    dispatch(customersLoading(true));
    axios.get('/api/customers')
      .then(res => dispatch(setCustomers(res.data)))
      .catch(err => dispatch(apiError(errorMessage(err))))
      .finally(() => dispatch(customersLoading(false)));
  }
}

export function removeCustomer(id) {
  return function(dispatch) {
    axios.delete(`/api/customers/${id}/`)
      .then(res => dispatch(fetchCustomers()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function addTemplate(template) {
  return function(dispatch) {
    let { variables } = template;
    debugger;
    delete template["variables"];
    axios.post(`/api/templates/`, template)
      .then(res =>
        Promise.all(
          variables.map(v =>
            axios.post(`/api/variables/`, {...v, template: res.data["id"]})
          )
        ).then(_ => dispatch(fetchTemplates()))
        .catch(err => dispatch(apiError(errorMessage(err))))
      ).catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function updateTemplate(template) {
  return function(dispatch) {
    delete(template.variables);
    axios.put(`/api/templates/${template.id}/`, template)
      .then(res => dispatch(fetchTemplates()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function fetchTemplates() {
  return function(dispatch) {
    dispatch(templatesLoading(true));
    axios.get('/api/templates')
      .then(res => dispatch(setTemplates(res.data)))
      .catch(err => dispatch(apiError(errorMessage(err))))
      .finally(() => dispatch(templatesLoading(false)));
  }
}

export function removeTemplate(id) {
  return function(dispatch) {
    axios.delete(`/api/templates/${id}/`)
      .then(res => dispatch(fetchTemplates()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function addVariable(template, name) {
  return function(dispatch) {
    axios.post('/api/variables/', {template: template.id, name: name})
      .then(res => dispatch(fetchTemplates()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function removeVariable(id) {
  return function(dispatch) {
    axios.delete(`/api/variables/${id}/`)
      .then(res => dispatch(fetchTemplates()))
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function fetchServices() {
  return function(dispatch) {
    dispatch(servicesLoading(true));
    axios.get('/api/services')
      .then(res => dispatch(setServices(res.data)))
      .catch(err => dispatch(apiError(errorMessage(err))))
      .finally(() => dispatch(servicesLoading(false)));
  }
}

export function removeService(id) {
  return function(dispatch) {
    axios.delete(`/api/services/${id}/`)
      .then(res => { dispatch(fetchServices()); dispatch(fetchCPEs()); })
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function updateService(id, data) {
  return function(dispatch) {
    data["values"] = Object.values(data["values"]);
    axios.put(`/api/services/${id}/`, data)
      .then(res => { dispatch(fetchServices()); dispatch(fetchCPEs()); })
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}

export function addService(cpeId, data) {
  return function(dispatch) {
    let values = data["values"];
    delete data["values"];
    data["cpe"] = cpeId;
    axios.post(`/api/services/`, data)
      .then(res =>
        Promise.all(
          res.data["values"].map(v =>
            axios.patch(`/api/values/${v.id}/`, values[v["variable"]["name"]])
          )
        ).then(_ => {
          dispatch(fetchServices());
          dispatch(fetchCPEs());
        }).catch(err => dispatch(apiError(errorMessage(err))))
      )
      .catch(err => dispatch(apiError(errorMessage(err))));
  }
}
