import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import {
  Alert,
  Navbar,
  NavbarBrand,
  NavbarText,
  NavLink,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem
} from "reactstrap";

import CPEs from './components/cpes';
import CPE from './components/cpe';
import Customers from './components/customers';
import Customer from './components/customer';
import Templates from './components/templates';
import Template from './components/template';
import Service from './components/service';
import SearchBar from './components/searchbar';
import SearchResults from './components/searchresults';
import { fetchCPEs, fetchCustomers, fetchTemplates, clearAPIError } from './redux/actions';


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      toggle: false,
      searchBar: '',
    };
  }

  componentDidMount() {
    this.props.fetchCPEs();
    this.props.fetchCustomers();
    this.props.fetchTemplates();
  }

  onDismiss = () => {
    this.props.clearAPIError();
  }

  toggle = () => {
    const toggle = !this.state.toggle;
    this.setSate({toggle});
  }

  renderSearchbar() {
    return (
      <Switch>
        <Route path="/" render={(props) =>
          <SearchResults {...props}
                         forSearch={this.state.searchBar}
                         resetSearch={() => this.setState({searchBar: ''})}/>
        }/>
      </Switch>
    );
  }

  renderRoutes() {
    return (
      <Switch>
        <Route path="/customers/new" component={Customer}/>
        <Route path="/customers/:customerId" component={Customer}/>
        <Route path="/customers">
          <Customers/>
        </Route>
        <Route path="/templates/new" component={Template}/>
        <Route path="/templates/:templateId" component={Template}/>
        <Route path="/templates">
          <Templates/>
        </Route>
        <Route path="/cpes/new" component={CPE}/>
        <Route path="/cpes/:cpeId" component={CPE}/>
        <Route path="/services/:cpeId/:serviceId" component={Service}/>
        <Route path="/services/:cpeId" component={Service}/>
        <Route path="/"><CPEs/></Route>
      </Switch>
    );
  }

  render() {
    const isOpen = this.state.toggle;

    return (
      <Router>
        <Navbar color="white" light expand="md">
          <NavbarBrand href="/"><img className="logo" alt="logo" src="/logo.svg"/></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink><Link to="/">Devices</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/customers">Customers</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/templates">Service Templates</Link></NavLink>
              </NavItem>
            </Nav>
            <NavbarText>
              <SearchBar placeholder="Search" onChange={(e) => this.setState({searchBar: e.target.value})} value={this.state.searchBar}/>
            </NavbarText>
          </Collapse>
        </Navbar>
        <main className="content">
          { this.props.error === null ? "" : <Alert color="danger" toggle={this.onDismiss}>{this.props.error}</Alert> }
          {this.state.searchBar ?
            this.renderSearchbar() :
            this.renderRoutes()}
        </main>
      </Router>
    );
  }
}

export default connect(
  (state) => ({
    error: state.api.error,
  }),
  {fetchCPEs, fetchCustomers, fetchTemplates, clearAPIError}
)(App);
